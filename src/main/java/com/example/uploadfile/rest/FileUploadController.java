package com.example.uploadfile.rest;

import com.example.uploadfile.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
public class FileUploadController {

    @Autowired
    private FileUploadService fileUploadService;

    @PostMapping("/upload/db")
    public void uploadDb(@RequestParam("file") MultipartFile multipartFile) throws IOException {
            fileUploadService.uploadToDb(multipartFile);
    }

    @GetMapping
    public String hi(){
        return "hi";
    }
}
