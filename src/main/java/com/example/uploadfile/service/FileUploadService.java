package com.example.uploadfile.service;

import com.example.uploadfile.model.UploadedFile;
import com.example.uploadfile.repository.FileUploadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class FileUploadService {

    @Autowired
    private FileUploadRepository fileUploadRepository;

    public void uploadToDb(MultipartFile multipartFile) throws IOException {
        UploadedFile uploadedFile = new UploadedFile();
        uploadedFile.setFileData(multipartFile.getBytes());
        uploadedFile.setFileType(multipartFile.getContentType());
        uploadedFile.setFileName(multipartFile.getOriginalFilename());
        fileUploadRepository.save(uploadedFile);
    }

}
